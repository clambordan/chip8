export const Config = {
  canvasWidth: 750,
  canvasHeight: 500,
  canvasColor: "#FFC0CB",  // "pink"
  // initialGridLength: 961,
  // gridShiftFrequency: 40,  // milliseconds
  initialGridLength: 9,
  gridShiftFrequency: 0,  // milliseconds
};