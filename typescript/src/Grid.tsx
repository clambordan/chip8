import React from 'react';
import { selectGrid } from './GameState';
import { useSelector } from 'react-redux';
import { drawSquare } from './Square';

interface GridProps {
  canvasRef: React.RefObject<HTMLCanvasElement>;
  padding: [number, number];
}

const Grid: React.FC<GridProps> = ({ canvasRef, padding }) => {
  const grid = useSelector(selectGrid);
  const drawGrid = () => {
    const canvas = canvasRef.current;
    if (canvas) {
      const ctx = canvas.getContext('2d');
      if (ctx) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        grid.squares.forEach((square, i) => {
          const row = Math.floor(i / grid.numRows);
          const col = i % grid.numRows;
          drawSquare(canvas,
                     col * grid.squareSize + padding[0],
                     row * grid.squareSize + padding[1],
                     grid.squareSize,
                     grid.squareSize,
                     square.color);
        });
      }
    }
  };
  return <>{drawGrid()}</>;
};

export default Grid;
