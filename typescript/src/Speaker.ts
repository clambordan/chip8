// Speaker.ts

class Speaker {
  private audioCtx: AudioContext;
  private gain: GainNode;
  private oscillator: OscillatorNode;
  private isPlaying: boolean;

  constructor() {
    const AudioContext = window.AudioContext || (window as any).webkitAudioContext;
    this.audioCtx = new AudioContext();

    // Create a gain node and connect it to the destination
    this.gain = this.audioCtx.createGain();
    this.gain.connect(this.audioCtx.destination);

    // Create an oscillator but don't start it yet
    this.oscillator = this.audioCtx.createOscillator();
    this.oscillator.type = 'square';
    this.oscillator.connect(this.gain);
    
    // Initially, the oscillator is not playing
    this.isPlaying = false;
  }

  ensureAudioContextState(): Promise<void> {
    if (this.audioCtx.state === 'suspended') {
      return this.audioCtx.resume();
    }
    return Promise.resolve();
  }

  play(frequency: number = 440): void {
    this.ensureAudioContextState().then(() => {
      if (!this.isPlaying) {
        // Start the oscillator if it's not already playing
        // console.log("Starting oscillator...");
        this.oscillator.start();
        this.isPlaying = true;
      }
      // Only update the frequency if it's different to minimize work
      if (this.oscillator.frequency.value !== frequency) {
        // console.log("Setting oscillator frequency...");
        this.oscillator.frequency.setValueAtTime(frequency, this.audioCtx.currentTime);
      }
      // Ensure the gain is set to 1 to make the sound audible
      this.gain.gain.setValueAtTime(.05, this.audioCtx.currentTime);
      // console.log("Played!");
    });
  }

  stop(): void {
    // Instead of stopping the oscillator, reduce the gain to 0 to "stop" the sound
    if (this.isPlaying) {
      this.gain.gain.setValueAtTime(0, this.audioCtx.currentTime);
      // console.log("Stopped!");
    }
  }
}

export default Speaker;
