import { store, shiftGridHorizontal } from './GameState';

describe('gridSlice reducer', () => {
  it('should handle initial state', () => {
    expect(store.getState().grid).toBeDefined();
  });

  it('should handle shiftGridHorizontal', () => {
    const initialState = store.getState().grid;
    
    // Dispatch the action
    store.dispatch(shiftGridHorizontal(1));
    
    const newState = store.getState().grid;

    // Assert that the grid state has changed as expected
    expect(newState).not.toEqual(initialState);
    // Additional specific tests for the logic of shiftGridHorizontal
  });

  // ... other tests
});
