// Chip8Emulator.tsx

import React, { useEffect, useState } from 'react';
import Chip8 from './Chip8';
import Chip8Canvas from './Chip8Canvas';
import Keyboard from './Keyboard';
import Speaker from './Speaker';
// import gameURL from './programs/IBM.c8';
import gameURL from './programs/7-beep.ch8';
// import gameURL from './programs/3-corax.ch8';
import StatePrintout from './StatePrintout';

const Chip8Emulator: React.FC = () => {
    const [hasStarted, setHasStarted] = useState(false);
    const [chip8] = useState(new Chip8());
    const [screenData, setScreenData] = useState(chip8.getScreenData());
    const [keyboard] = useState(new Keyboard());
    const [speaker, setSpeaker] = useState<Speaker|null>(null);

    useEffect(() => {
        if (!hasStarted) { return; }
        // Fetch and load the default game
        fetch(gameURL)
            .then(response => {
              // console.log('Response MIME type:', response.headers.get('Content-Type'));
              if (response.ok) {
                  return response.arrayBuffer();
              }
              throw new Error(`Failed to load game: ${response.statusText}`);
            })
            .then(buffer => {
                const gameData = new Uint8Array(buffer);
                // console.log('Buffer:', buffer);
                // console.log('GameData:', gameData);
                // const numArray = Array.from(gameData.slice(0));
                // console.log('GameData ASCII:', String.fromCharCode.apply(null, numArray));
                chip8.loadProgram(gameData);
                startEmulation();
            })
            .catch(error => console.error('Failed to load game:', error));

            const startEmulation = () => {
              let lastChip8UpdateTime = 0;
              let lastScreenUpdateTime = 0;
              let lastTimerUpdateTime = 0;
              const chip8UpdateInterval = 1000 / 700;
              // const chip8UpdateInterval = 1000 / 1;  // TODO remove DEBUG
              const screenUpdateInterval = 1000 / 60;
              // const timerUpdateInterval = 1000 / 60;
              const timerUpdateInterval = 1000 / 1;
              const loop = (timestamp: number) => {
                  const timeSinceLastChip8Update = timestamp - lastChip8UpdateTime;
                  const timeSinceLastScreenUpdate = timestamp - lastScreenUpdateTime;
                  const timeSinceLastTimerUpdate = timestamp - lastTimerUpdateTime;
                  if (timeSinceLastChip8Update >= chip8UpdateInterval) {
                      // Update Chip-8 logic
                      chip8.update(); // Update Chip8 state
                      lastChip8UpdateTime = timestamp - (timeSinceLastChip8Update % chip8UpdateInterval);
                  }
                  if (timeSinceLastScreenUpdate >= screenUpdateInterval) {
                      let newScreenData = chip8.getScreenData();
                      setScreenData(newScreenData);
                      lastScreenUpdateTime = timestamp - (timeSinceLastScreenUpdate % screenUpdateInterval);
                  }
                  if (timeSinceLastTimerUpdate >= timerUpdateInterval) {
                      let soundOn = chip8.updateTimers();
                      if (soundOn) {
                        // console.log("Sound on!");
                        // console.log("Speaker: ", speaker);
                        speaker?.play();
                      } else {
                        // console.log("Sound off!");
                        speaker?.stop();
                      }
                      lastTimerUpdateTime = timestamp - (timeSinceLastTimerUpdate % timerUpdateInterval);
                  }
                  requestAnimationFrame(loop);
              };
              requestAnimationFrame(loop);
          };          
    }, [chip8, hasStarted]);

    // useEffect(() => {
    //     console.log("Updated screenData:", screenData);
    // }, [screenData]);

    return (
      <>
        {hasStarted
          ?
          (<Chip8Canvas
            screenData={screenData}
            style={{
                margin: "0.5rem"
            }}
          />)
          :
          <button onClick={()=>{
            // Wait for user interaction before introducing audio
            setHasStarted(true);
            setSpeaker(new Speaker());
          }}>Start Emulator</button>
        }
        <StatePrintout {...chip8.getState()}/>
      </>
    );
};

export default Chip8Emulator;
