import { Config } from './Config';

export const drawSquare = (canvas: HTMLCanvasElement, x: number, y: number, width: number, height: number, color: string) => {
  const ctx = canvas.getContext('2d');
  if (ctx) {
    // ctx.imageSmoothingEnabled = false;
    ctx.fillStyle = color;
    ctx.fillRect(x, y, width, height);
  }
};

export const getRandomColorHex = (): string => {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

export const getFilteredRandomColorHex = (): string => {
  while (true) {
    let color = getRandomColorHex();
    if (color !== Config.canvasColor) {
      return color;
    }
  }
};
