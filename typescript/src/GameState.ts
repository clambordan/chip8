import { configureStore, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Config } from './Config';
import { getFilteredRandomColorHex } from './Square';

interface SquareState {
  clicks: 0;
  color: string;
}

interface GridState {
  numRows: number;
  shiftLeftFrequency: number;
  squareSize: number;
  squares: SquareState[];
}

interface InputState {
  clicks: Record<string, number>;
}

const calculateInitialGridState = (): GridState => {
  const canvasSize = Math.min(Config.canvasWidth, Config.canvasHeight);
  const numGridRows = Math.ceil(Math.sqrt(Config.initialGridLength));
  const squareSize = canvasSize / numGridRows;
  const squares = Array.from({ length: Config.initialGridLength }, (): SquareState => ({
    clicks: 0,
    color: getFilteredRandomColorHex(),
  }));
  return {
    numRows: numGridRows,
    shiftLeftFrequency: Config.gridShiftFrequency,
    squareSize: squareSize,
    squares: squares,
  };
};

const gridInitialState = calculateInitialGridState();

const reinitializeSquare = (square: SquareState) => {
  square.color = getFilteredRandomColorHex();
  square.clicks = 0;
  return square;
}

const gridSlice = createSlice({
  name: 'grid',
  initialState: gridInitialState,
  reducers: {
    squareClicked: (gridState, action: PayloadAction<number>) => {
      const index = action.payload;
      // console.log(`Square index clicked: ${index}`);
      if (index < 0 || index >= gridState.squares.length) { return; }
      gridState.squares[index].clicks++;
    },
    shiftGridHorizontal: (gridState, action: PayloadAction<number>) => {
      const shiftAmount = action.payload;
      const totalSquares = gridState.squares.length;
      const numRows = gridState.numRows;
      const numCols = gridState.numRows;
      if (shiftAmount === 0 || totalSquares === 0) return;
      // Calculate effective shift respecting the grid dimensions
      const effectiveShift = Math.abs(shiftAmount) % numRows;
      if (effectiveShift === 0) {
        return;
      } else if (effectiveShift >= numCols) {

      }
      // Determine direction of shift
      const shiftDirection = shiftAmount > 0 ? -1 : 1;
      for (let row = 0; row < numRows; row++) {
        // Calculate start and end indices for each row
        const startIndex = row * numCols;
        const endIndex = startIndex + numCols;
        const rowSquares = gridState.squares.slice(startIndex, endIndex);
        // Perform shift operation
        for (let i = 0; i < effectiveShift; i++) {
          const index = shiftDirection === -1 ? rowSquares.length - 1 : 0;
          const shiftedSquare = rowSquares.splice(index, 1)[0];
          reinitializeSquare(shiftedSquare);
          if (shiftDirection === -1) {
            rowSquares.unshift(shiftedSquare);
          } else {
            rowSquares.push(shiftedSquare);
          }
        }
        // Update the gridState for this row
        gridState.squares.splice(startIndex, numRows, ...rowSquares);
      }
    },
    // Additional grid-related reducers
  },
});

const inputInitialState: InputState = {
  clicks: {},
};

const inputSlice = createSlice({
  name: 'input',
  initialState: inputInitialState,
  reducers: {
    updateClicks: (state, action: PayloadAction<string>) => {
      const id = action.payload;
      state.clicks[id] = (state.clicks[id] || 0) + 1;
    },
    // Additional input-related reducers
  },
});

export const store = configureStore({
  reducer: {
    input: inputSlice.reducer,
    grid: gridSlice.reducer,
  },
});

export const { shiftGridHorizontal, squareClicked } = gridSlice.actions;
export const { updateClicks } = inputSlice.actions;

export type RootState = ReturnType<typeof store.getState>;

export const selectRoot =  (state: RootState) => state;
export const selectGrid =  (state: RootState) => state.grid;
export const selectInput = (state: RootState) => state.input;
