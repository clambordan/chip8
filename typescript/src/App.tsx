import React from 'react';
import { Provider } from 'react-redux';
import { store } from './GameState';
// import StatePrintout from './StatePrintout';
import Chip8Emulator from './Chip8Emulator';

const App = () => {
  return (
    <Provider store={store}>
      <Chip8Emulator />
      {/* <StatePrintout /> */}
    </Provider>
  );
};

export default App;
