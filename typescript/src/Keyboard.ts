// Keyboard.ts

const KEYMAP_1234_QWER_ASDF_ZXCV = {
  49: 0x1, // 1
  50: 0x2, // 2
  51: 0x3, // 3
  52: 0xC, // 4
  81: 0x4, // Q
  87: 0x5, // W
  69: 0x6, // E
  82: 0xD, // R
  65: 0x7, // A
  83: 0x8, // S
  68: 0x9, // D
  70: 0xE, // F
  90: 0xA, // Z
  88: 0x0, // X
  67: 0xB, // C
  86: 0xF  // V
}

class Keyboard {
    private KEYMAP: { [keycode: number]: number };
    private keysPressed: boolean[];
    private onNextKeyPress: ((key: number) => void) | null;

    constructor() {
        this.KEYMAP = KEYMAP_1234_QWER_ASDF_ZXCV;
        this.keysPressed = [];
        this.onNextKeyPress = null;
        window.addEventListener('keydown', this.onKeyDown.bind(this), false);
        window.addEventListener('keyup', this.onKeyUp.bind(this), false);
    }

    isKeyPressed(keyCode: number): boolean {
        return !!this.keysPressed[this.KEYMAP[keyCode]];
    }

    private onKeyDown(event: KeyboardEvent): void {
        let key = this.KEYMAP[event.which];
        if (typeof key !== 'undefined') {
            this.keysPressed[key] = true;
            if (this.onNextKeyPress !== null && key !== undefined) {
                this.onNextKeyPress(key);
                this.onNextKeyPress = null;
            }
        }
    }

    private onKeyUp(event: KeyboardEvent): void {
        let key = this.KEYMAP[event.which];
        if (typeof key !== 'undefined') {
            this.keysPressed[key] = false;
        }
    }
}

export default Keyboard;