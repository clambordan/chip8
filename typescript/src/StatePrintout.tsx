import React from 'react';
import { useSelector } from 'react-redux';
import { selectRoot } from './GameState';

/**
 * Recursively sort the keys of an object alphabetically.
 * @param obj - The object to sort the keys of.
 * @returns A new object with sorted keys.
 */
function sortObjectKeys(obj: any): any {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }
  if (Array.isArray(obj)) {
    return obj.map(sortObjectKeys);
  }
  const sortedObj: { [key: string]: any } = {};
  // Sort keys alphabetically
  const sortedKeys = Object.keys(obj).sort();
  for (const key of sortedKeys) {
    sortedObj[key] = sortObjectKeys(obj[key]);
  }
  return sortedObj;
}

const StatePrintout: React.FC = (state: unknown) => {
  const sortedState = sortObjectKeys(state);
  // sortedState.grid.squares = JSON.stringify(sortedState.grid.squares, null)
  return <pre>{JSON.stringify(sortedState, null, 2)}</pre>;
  // return <pre>{JSON.stringify(sortedState, null, 2)}</pre>;
};

export default StatePrintout;
