// Chip8.ts

import Chip8Font from './Chip8Font'
import Speaker from './Speaker';

export default class Chip8 {
  private static readonly INTERNAL_MEMORY_SIZE: number = 512;
  private static readonly MEMORY_SIZE: number = 4096;
  private MEMORY: Uint8Array;
  private REGISTERS: Uint8Array;
  private PC: Uint16Array;  // Program Counter
  private I: Uint16Array;  // Index Counter
  private STACK: number[];
  private delayTimer: Uint8Array;
  public soundTimer: Uint8Array;  // TODO private
  private displayWidth: number;
  private displayHeight: number;
  private DISPLAY: boolean[];
  private lastUpdateLogTime: number;
  private sound: HTMLAudioElement;

  constructor(displayWidth=64) {
    this.MEMORY     = new Uint8Array(Chip8.MEMORY_SIZE);
    this.REGISTERS  = new Uint8Array(16);
    this.delayTimer = new Uint8Array(1);
    this.soundTimer = new Uint8Array(1);
    this.STACK = [];
    this.PC    = new Uint16Array(1);
    this.I     = new Uint16Array(1);
    this.displayWidth = displayWidth;
    this.displayHeight = displayWidth / 2;
    this.DISPLAY = new Array(this.displayWidth*this.displayHeight).fill(false);
    // this.sound https://stackoverflow.com/a/23395136
    this.sound = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");  
    this.DISPLAY[596] = true;  // TODO remove
    this.DISPLAY[591] = true;  // TODO remove
    this.loadFont();
    this.lastUpdateLogTime = Date.now()-10000;
  }

  loadFont(): void {
    this.MEMORY.set(Chip8Font, 0x0);
  }

  loadProgram(program: Uint8Array): void {
    if (program.length > (Chip8.MEMORY_SIZE - Chip8.INTERNAL_MEMORY_SIZE)) {
        throw new Error("Program size exceeds available memory space.");
    }
    // console.log(this.MEMORY[this.PC[0]]);
    this.MEMORY.set(program, 0x200);
    this.PC[0] = 0x200;
    // console.log(this.MEMORY.slice(0x200));
    // console.log(`Program loaded!  PC: 0x${this.PC[0].toString(16)}`);
    // console.log(this.MEMORY.slice(0x200));
    // console.log(this.MEMORY[this.PC[0]]);
  }

  private hexify(n: number, padAmount=2) {
    return n.toString(16).padStart(padAmount, '0');
  }

  public getState(): object {
    return {
      // MEMORY: this.formatMemoryAsGrid().split('\n'),
      REGISTERS: Array.from(this.REGISTERS).map(r => this.hexify(r)).join(' '),
      PC: this.hexify(this.PC[0], 4),
      I: this.hexify(this.I[0], 4),
      STACK: "" + Array.from(this.STACK).map(this.hexify, this).join(" "),
      delayTimer: this.hexify(this.delayTimer[0]),
      soundTimer: this.hexify(this.soundTimer[0]),
      displayWidth: this.displayWidth,
      displayHeight: this.displayHeight,
      // DISPLAY: this.formatDisplayAsGrid().split('\n'),
      lastUpdateLogTime: this.lastUpdateLogTime,
    };
  }

  private formatMemoryAsGrid(): string {
    // const bytesPerLine = 128;
    const bytesPerLine = 64;
    let grid = '';
    for (let i = 0; i < this.MEMORY.length; i+=2) {
      grid += this.MEMORY[i].toString(16).padStart(2, '0').toUpperCase();
      grid += this.MEMORY[i+1].toString(16).padStart(2, '0').toUpperCase();
      if ((i+1) % bytesPerLine === 0 || (i+2) % bytesPerLine === 0) {
        grid += '\n';
      } else {
        grid += ' ';
      }
    }

    return grid;
  }

  private getRandomNumber(maxInclusive=0xFF): number {
    let maxExclusive = maxInclusive + 1;
    return Math.random() * maxExclusive | 0;
  }

  private formatDisplayAsGrid(): string {
    let grid = '';
    for (let y = 0; y < this.displayHeight; y++) {
      for (let x = 0; x < this.displayWidth; x++) {
        const index = this.displayXYToIndex(x, y);
        grid += this.DISPLAY[index] ? '1' : '0';
      }
      grid += '\n';
    }
    return grid;
  }

  displayIndexToXY(index:number): [number, number] {
    let x = index % this.displayWidth;
    let y = (index-x) / this.displayWidth;
    return [x, y];
  }
  displayXYToIndex(x:number, y:number): number {
    return y*this.displayWidth + x;
  }

  getScreenData(): boolean[] { return [...this.DISPLAY]; }
  setFlagRegister(value: number): void { this.REGISTERS[0xF] = value; }

  update(): void {
    const now = Date.now();
    const highByte = this.MEMORY[this.PC[0]]
    const lowByte  = this.MEMORY[this.PC[0]+1];
    if (highByte === undefined || lowByte === undefined) {
      console.log("Undefined!");
      return;
    }
    const instruction = (highByte<<8) | lowByte;
    const nibbles = [highByte>>4, 0xF&highByte, lowByte>>4, 0xF&lowByte]
    const X = nibbles[1];
    const Y = nibbles[2];
    const vX = this.REGISTERS[X];
    const vY = this.REGISTERS[Y];
    const   N = nibbles[3];
    const  NN = lowByte;
    const NNN = (nibbles[1]<<8) | lowByte;
    this.PC[0] += 2;

    const logInstruction = false;
    const debugUpdate = () => {
      // if (now - this.lastUpdateLogTime >= 1000) {
        // console.log("High nibble: 0x" + highByte.toString(16));
        // console.log("Low nibble:  0x" + lowByte.toString(16));
        // console.log("Instruction: 0x" + instruction.toString(16));
        console.log({
          "High byte": "0x" + this.hexify(highByte),
          "Low byte":  "0x" + this.hexify(lowByte),
          "Nibbles":  nibbles.map((nibble) => "0x" + this.hexify(nibble)),
          "Instruction": "0x" + this.hexify(instruction),
        });
        this.lastUpdateLogTime = now;
      // }
    }
    // debugUpdate();

    switch (nibbles[0]) {  // High nibble
      case (0x0):
        switch (instruction) {
          case 0x00E0: // 00E0 clear screen
            if (logInstruction) console.log("00E0 - Clear screen");
            this.DISPLAY = this.DISPLAY.map((_)=>false)
            break;
          case 0x00EE: // 00EE Return from subroutine
            if (logInstruction) { console.log("00E0 - Return from subroutine"); }
            this.PC[0] = this.STACK.pop() ?? -1;  // TODO handle default
            break;
        }
        break;

      case (0x1):  // 1NNN - jump
        if (logInstruction) { console.log(`1NNN - Jump to ${this.hexify(NNN, 4)}`); }
        this.PC[0] = NNN;
        break;

      case (0x2):  // 2NNN - Call subroutine
        if (logInstruction) { console.log(`2NNN - Call subroutine at ${this.hexify(NNN, 4)}`); }
        this.STACK.push(this.PC[0]);
        this.PC[0] = NNN;
        break;

      case (0x3):  // 3XNN - Skip if equals
        if (logInstruction) { console.log(`3XNN - Skip if vX (${this.hexify(vX)}) === NN (${this.hexify(NN)})`); }
        if (vX === NN) {
          this.PC[0] += 2;
        }
        break;

      case (0x4):  // 4XNN - Skip if not equals
        if (logInstruction) { console.log(`4XNN - Skip if vX (${this.hexify(nibbles[1])}) !== NN (${this.hexify(lowByte)})`); }
        if (this.REGISTERS[X] !== lowByte) {
          this.PC[0] += 2;
        }
        break;

      case (0x5):  // 5XY0 - Skip if equals
        if (logInstruction) { console.log(`5XY0 - Skip if vX (${this.hexify(vX)}) === vY (${this.hexify(vY)})`); }
        if (vX === vY) {
          this.PC[0] += 2;
        }
        break;

      case (0x6):  // 6XNN - set register VX
        if (logInstruction) { console.log(`6XNN - Set V[${nibbles[1].toString(16)}]`); }
        this.REGISTERS[X] = lowByte;
        break;

      case (0x7):  // 7XNN - add value to register VX
        if (logInstruction) { console.log(`7XNN - Add ${this.hexify(NN)} to v[${this.hexify(nibbles[1])}]`); }
        this.REGISTERS[X] += lowByte;
        break;

      case (0x8):
        switch (N) {
          case (0x0): // 8XY0 - Set vX = vY
            if (logInstruction) { console.log(`8XY0 - Set vX (v[${nibbles[1]}]) = vY (${vY})`); }
            this.REGISTERS[nibbles[1]] = vY;
            break;
          case (0x1): // 8XY1 - Set vX = vX|vY
            if (logInstruction) { console.log(`8XY1 - Set vX (v[${nibbles[1]}]) = vX | vY (${vY})`); }
            this.REGISTERS[nibbles[1]] = vX | vY;
            break;
          case (0x2): // 8XY2 - Set vX = vX&vY
            if (logInstruction) { console.log(`8XY2 - Set vX (v[${nibbles[1]}]) = vX & vY (${vY})`); }
            this.REGISTERS[nibbles[1]] = vX & vY;
            break;
          case (0x3): // 8XY3 - Set vX = vX^vY
            if (logInstruction) { console.log(`8XY3 - Set vX (v[${nibbles[1]}]) = vX ^ vY (${vY})`); }
            this.REGISTERS[nibbles[1]] = vX ^ vY;
            break;
          case (0x4): // 8XY4 - Set vX += vY
            if (logInstruction) { console.log(`8XY1 - Set vX (v[${nibbles[1]}]) = vX + vY (${vY})`); }
            let sum = vX + vY;
            this.REGISTERS[X] = sum;
            this.setFlagRegister(sum > 255 ? 1 : 0)  // 1 if overflow
            break;
          case (0x5): // 8XY5 - Set vX -= vY
            if (logInstruction) { console.log(`8XY1 - Set vX (v[${nibbles[1]}]) = vX - vY (${vY})`); }
            let difference = vX - vY;
            this.REGISTERS[X] = difference;
            this.setFlagRegister(difference < 0 ? 0 : 1)  // 0 if underflow
            break;
          case (0x7): // 8XY7 - Set vX = vY - vX
            if (logInstruction) { console.log(`8XY1 - Set vX (v[${nibbles[1]}]) = vY (${vY}) - vX`); }
            let reverseDifference = vY - vX;
            this.REGISTERS[X] = reverseDifference;
            this.setFlagRegister(reverseDifference < 0 ? 0 : 1)  // 0 if underflow
            break;
          case (0x6): // 8XY6 - Set vX = (vY >> 1)
            if (logInstruction) { console.log(`8XY1 - Set vX (v[${nibbles[1]}]) = vY (${vY}) >> 1`); }
            let rightShiftedBit = 1 & vY;
            this.REGISTERS[X] = (vY >> 1);
            this.setFlagRegister(rightShiftedBit);
            break;
          case (0xE): // 8XYE - Set vX = (vY << 1)
            if (logInstruction) { console.log(`8XY1 - Set vX (v[${nibbles[1]}]) = vY (${vY}) << 1`); }
            let leftShiftedBit = vY >> 7;
            this.REGISTERS[X] = (vY << 1);
            this.setFlagRegister(leftShiftedBit);
            break;
        }
        break;

      case (0x9):  // 9XY0 - Skip if not equals
        if (logInstruction) { console.log(`5XY0 - Skip if vX (${this.hexify(vX)}) !== vY (${this.hexify(vY)})`); }
        if (vX !== vY) {
          this.PC[0] += 2;
        }
        break;

      case (0xA):  // ANNN - set index register I
        if (logInstruction) { console.log(`ANNN - Set Index register to ${NNN}`); }
        this.I[0] = NNN;
        break;

      case (0xB):  // BNNN - Jump to address plus offset
        console.log(`BNNN - Jump to NNN (${NNN}) + v0`);
        this.PC[0] = NNN + this.REGISTERS[0];
        break;

      case (0xC):  // CXNN - Set vX = NN & rand()
        console.log(`CXNN - Set v[${X}] = NN (${NN}) & rand()`);
        this.REGISTERS[X] = NN & this.getRandomNumber(0xFF);
        break;

      case (0xD):  // DXYN - display/draw
        // TODO truncate sprites at edge of screen.  Don't wrap.
        if (logInstruction) { console.log(`DXYN - Draw ${nibbles[3].toString(16).padStart(2, "0")}-height sprite at (${nibbles[1]}, ${nibbles[2]})`); }
        const x = this.REGISTERS[X] % this.displayWidth;
        const y = this.REGISTERS[Y] % this.displayHeight;
        let pixelFlipped = false;
        for (let byteIndex = 0; byteIndex < N; byteIndex++) {
          const spriteByte = this.MEMORY[this.I[0] + byteIndex];
          // const spriteByteBinary = spriteByte.toString(2).padStart(8, '0');
          // console.log(`Sprite byte [${this.hexify(byteIndex)}]: ${spriteByteBinary}`);
          for (let bitIndex = 0; bitIndex < 8; bitIndex++) {
            const spritePixel = (spriteByte & (0x80 >> bitIndex)) !== 0;
            const screenX = (x + bitIndex) % this.displayWidth;
            const screenY = (y + byteIndex) % this.displayHeight;
            const screenIndex = this.displayXYToIndex(screenX, screenY);
            let bitLog = `(${screenX}, ${screenY}) (index ${screenIndex})`;
            if (spritePixel) {
              if (this.DISPLAY[screenIndex]) {
                pixelFlipped = true;
              }
              let before = this.DISPLAY[screenIndex] ? 1 : 0;
              this.DISPLAY[screenIndex] = this.DISPLAY[screenIndex] ? false : true;
              bitLog = `${before}->${this.DISPLAY[screenIndex]?1:0} ${bitLog}`;
            } else {
              bitLog = `${this.DISPLAY[screenIndex]?1:0}->${this.DISPLAY[screenIndex]?1:0} ${bitLog}`;
            }
            // console.log(bitLog);
          }
        }
        this.setFlagRegister(pixelFlipped ? 1 : 0);
        break;

      case (0xF):
        switch (lowByte) {
          case (0x65): // FX65 - Read X+1 bytes into consecutive registers
            if (logInstruction) { console.log(`FX65 - Read X(${nibbles[1]}) bytes from I into registers.`); }
            for (let i=0; i<=X; i++) {
              this.REGISTERS[i] = this.MEMORY[this.I[0] + i];
            }
            this.I[0] += X + 1; // TODO toggle quirk?
            break;
          case (0x55): // FX55 - Write X+1 bytes from consecutive registers to I
            if (logInstruction) { console.log(`FX65 - Write X(${nibbles[1]}) bytes from registers to I.`); }
            for (let i=0; i<=X; i++) {
              this.MEMORY[this.I[0] + i] = this.REGISTERS[i] ;
            }
            this.I[0] += X + 1; // TODO toggle quirk?
            break;
          case (0x33): // FX33 - Store decimal digits for vX in memory at I, I+1, I+2.
            if (logInstruction) { console.log(`FX33 - Store decimal digits for vX (${nibbles[1]} at I, I+1, I+2)`); }
            let decimal100 = Math.floor(vX / 100);
            let decimal10  = Math.floor((vX%100) / 10);
            let decimal1   = vX % 10;
            this.MEMORY[this.I[0]]   = decimal100;
            this.MEMORY[this.I[0]+1] = decimal10;
            this.MEMORY[this.I[0]+2] = decimal1;
            break;
          case (0x1E): // FX1E - I += vX
            if (logInstruction) { console.log(`FX1E - I += vX (v[${nibbles[1]}]`); }
            this.I[0] += vX;
            break;
        }
        break;

      default:
        // throw new Error(`Unhandled instruction 0x${instruction?.toString(16)}`);
        // console.log("Unhandled instruction.")
    }
  }

  beep(): void {
    this.sound.currentTime = 0;
    this.sound.play();
  }

  updateTimers(): boolean {
    if (this.delayTimer[0] > 0) {
      this.delayTimer[0] -= 1;
    }
    this.soundTimer[0] = 2;  //TODO remove
    if (this.soundTimer[0] > 0) {
      this.soundTimer[0] -= 1;
      return true;
    }
    return false;
  }
}
