import { drawSquare, getRandomColorHex, getFilteredRandomColorHex } from './Square';
import { Config } from './Config';

// Helper function to create a mock canvas context
const createMockCanvasContext = () => {
  const fillRect = jest.fn();
  const context = {
    fillRect,
    fillStyle: '',
    getContext: jest.fn()
  };
  context.getContext.mockReturnValue(context);
  const canvas = {
    getContext: jest.fn(() => context)
  } as unknown as HTMLCanvasElement;
  return { canvas, context, fillRect };
};

describe('Square utility functions', () => {
  describe('drawSquare', () => {
    it('draws a square on the canvas with correct style and dimensions', () => {
      const { canvas, context, fillRect } = createMockCanvasContext();
      const x = 10;
      const y = 20;
      const width = 30;
      const height = 40;
      const color = '#123456';

      drawSquare(canvas, x, y, width, height, color);

      expect(canvas.getContext).toHaveBeenCalledWith('2d');
      expect(fillRect).toHaveBeenCalledWith(x, y, width, height);
      // Check if fillStyle is correctly set
      expect(context.fillStyle).toBe(color);
    });
  });

  describe('Color generation functions', () => {
    it('getRandomColorHex returns a valid hex color', () => {
      const color = getRandomColorHex();
      expect(color).toMatch(/^#[0-9A-F]{6}$/i);
    });

    it('getFilteredRandomColorHex returns a valid hex color not equal to the canvas color', () => {
      const color = getFilteredRandomColorHex();
      expect(color).toMatch(/^#[0-9A-F]{6}$/i);
      expect(color).not.toBe(Config.canvasColor);
    });
  });
});
