// src/index.tsx
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css'; // If you have global styles
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root')!); // Non-null assertion for TypeScript

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
