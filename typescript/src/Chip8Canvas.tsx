import React, { useRef, useEffect } from 'react';
import { drawSquare } from './Square';

interface Chip8CanvasProps {
    screenData: boolean[];
    screenWidth?: number;
    [key: string]: unknown;
}

const Chip8Canvas: React.FC<Chip8CanvasProps> = ({ screenData, screenWidth=64, ...props }) => {
    const screenHeight = screenWidth / 2;
    const scale = 14;
    const canvasWidth = scale * screenWidth;
    const canvasHeight = scale * screenHeight;
    const canvasRef = useRef<HTMLCanvasElement>(null);

    const backgroundColor = "black";
    const foregroundColor = "white";
    const       gridColor = "pink";
    const gridThickness = 1;
    const gridOffset = gridThickness / 2;

    const draw = (ctx: CanvasRenderingContext2D) => {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        drawSquare(ctx.canvas, 0, 0, ctx.canvas.width, ctx.canvas.height, backgroundColor);
        screenData.forEach((element, index) => {
          let x = index % screenWidth;
          let y = Math.floor(index / screenWidth);
          drawSquare(ctx.canvas, scale*x, scale*y, scale, scale, gridColor)
          if (element) {
            drawSquare(ctx.canvas, scale*x+gridOffset, scale*y+gridOffset, scale-gridThickness, scale-gridThickness, foregroundColor)
          } else {
            drawSquare(ctx.canvas, scale*x+gridOffset, scale*y+gridOffset, scale-gridThickness, scale-gridThickness, backgroundColor)
          }
      });
    };

    useEffect(() => {
        const canvas = canvasRef.current;
        const context = canvas?.getContext('2d');
        if (context) {
            draw(context);
        }
    }, [screenData]);

    return <canvas
      ref={canvasRef}
      width={canvasWidth}
      height={canvasHeight}
      {...props}
    />;
};

export default Chip8Canvas;
